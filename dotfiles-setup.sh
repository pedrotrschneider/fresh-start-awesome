#!/bin/bash

# Home folder files
cp -r .moc ~/

# .config files
cp -r .config/alacritty ~/.config/
cp -r .config/arcolinux-betterlockscreen ~/.config/
cp -r .config/awesome ~/.config/
cp -r .config/devilspie2 ~/.config/
cp -r .config/fish ~/.config/
cp -r .config/kitty ~/.config/
cp -r .config/neofetch ~/.config/
cp -r .config/picom ~/.config/
cp -r .config/ranger ~/.config/
cp -r .config/rofi ~/.config/
cp -r .config/xournalpp ~/.config/

# .icons and .themes
cp -r .icons ~/
cp -r .themes ~/
sudo cp -r .themes/Arc-Horizon /usr/share/themes/

# wallpapers
cp -r wallpapers ~/Pictures/
sudo mkdir /usr/share/wallpapers
sudo cp Pictures/wallpapers/wallpaper.png /usr/share/wallpapers/
sudo cp .config/neofetch/stars2.png /usr/share/wallpapers/

# some program binaries
cp -r programs ~/Documents