#!/bin/bash

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

# Compositor stuff
run devilspie2
run picom --experimental-backends

# Some system tray stuff and xfce-session
run xfce4-session
run flameshot

# Animated wallpaper (comment this out to disable the animated wallpaper)
# run xwinwrap -g 1366x768 -ni -s -nf -b -un -ov -fdt -argb -- mpv --mute=yes --no-audio --no-osc --no-osd-bar --quiet --screen=0 --geometry=1366x768+0+0 -wid WID --loop ~/Pictures/wallpapers/your-name-light.mp4
# Normal wallpaper
feh --bg-fill ~/Pictures/wallpapers/wallpaper-your-name.jpg

# run roficlip.py --daemon
run numlockx on
