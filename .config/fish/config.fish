# Rust
set PATH $PATH ~/.cargo/bin

# Ranger
alias ranger "ranger --choosedir=$HOME/.rangerdir && cd (cat ~/.rangerdir)"
alias r "ranger"

# Jump
status --is-interactive; and source (jump shell fish | psub)

# Neofetch
alias neo="clear && neofetch"

# Git
alias gitcl="git clone"
alias gitst="git status"
alias gitpl="git pull"
alias gits="git status"
alias gita="git add"
alias gitc="git commit"
alias gitcm="git commit -m"
alias gitps="git push"
alias gitch="git checkout"

# Exa
alias ls="exa --long --header"

function nvm
   bass source $HOME/.nvm/nvm.sh --no-use ';' nvm $argv
end

# rsync
alias cp="rsync -ahz --progress"

# bat
alias cat="bat"

# theme
set -l foreground c0caf5
set -l selection 33467C
set -l comment 565f89
set -l red E95678
set -l orange ff9e64
set -l yellow FAB795
set -l green 29D398
set -l purple EE64AE
set -l cyan 59E3E3
set -l pink bb9af7

# Syntax Highlighting Colors
set -g fish_color_normal $foreground
set -g fish_color_command $green
set -g fish_color_keyword $pink
set -g fish_color_quote $yellow
set -g fish_color_redirection $foreground
set -g fish_color_end $orange
set -g fish_color_error $red
set -g fish_color_param $cyan
set -g fish_color_comment $comment
set -g fish_color_selection --background=$selection
set -g fish_color_search_match --background=$selection
set -g fish_color_operator $green
set -g fish_color_escape $pink
set -g fish_color_autosuggestion $comment

# Completion Pager Colors
set -g fish_pager_color_progress $comment
set -g fish_pager_color_prefix $cyan
set -g fish_pager_color_completion $foreground
set -g fish_pager_color_description $comment