#!/bin/bash

# Install paru AUR helper
cd
mkdir .source && cd .source
git clone https://aur.archlinux.org/paru.git paru
cd paru
makepkg -si
cd
paru -Syyu

# Install some useful libraries
paru -Sy libelf
