# Fresh Start Awesome

Repository containing scripts and dotfiles for a fresh start on Arch Linux with Awesome Window Manager.

## Xfce session and startup

To use Xfce with the Awesome window manager, some services need to be disabled on startup.

Under "current session", disable all programs, except:
- Xfce Settings Daemon
- Thunar
- msm_notifier
- Power Manager
- pulseaudio

Under "application startup", disable Xcape.

## Order to run the scripts:

1) initial-setup.sh
2) paru-setup.sh
3) dotfiles-setup.sh
4) software-setup.sh
5) codium-setup.sh
6) tp-link-archer-t2u-plus-driver-install.sh

## Post installation

- Configure GTK greeter settings

## Resources

- [Animated wallpaper](https://gist.github.com/CSaratakij/788261f1ebcf2aefa320255120f75efe)
- [Pokeget](https://github.com/talwat/pokeget)
- [Config I based mine on](https://github.com/drahenprofi/dotfiles)