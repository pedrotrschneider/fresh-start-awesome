#!/bin/bash

echo "MAKE SURE TO OPEN VSCODIUM ONCE BEFORE RUNNING THIS"
echo "Press enter to continue"
read

# Copy the config file
cp .config/codium/settings.json ~/.config/VSCodium/User

# Install extensions
# codium --install-extension .config/codium/codium-extensions/aaron-bond.better-comments-2.1.0.vsix
# codium --install-extension .config/codium/codium-extensions/CoenraadS.bracket-pair-colorizer-2-0.2.1.vsix
# codium --install-extension .config/codium/codium-extensions/HookyQR.beautify-1.5.0.vsix
# codium --install-extension .config/codium/codium-extensions/mhutchie.git-graph-1.30.0.vsix
# codium --install-extension .config/codium/codium-extensions/ms-python.python-2021.8.1159798656.vsix
# codium --install-extension .config/codium/codium-extensions/ms-vscode.cpptools-1.7.1.vsix
# codium --install-extension .config/codium/codium-extensions/MS-vsliveshare.vsliveshare-1.0.5065.vsix
# codium --install-extension .config/codium/codium-extensions/MS-vsliveshare.vsliveshare-pack-0.4.0.vsix
# codium --install-extension .config/codium/codium-extensions/PKief.material-icon-theme-4.9.0.vsix
# codium --install-extension .config/codium/codium-extensions/Vtrois.gitmoji-vscode-1.0.7.vsix
# codium --install-extension .config/codium/codium-extensions/xaver.clang-format-1.9.0.vsix