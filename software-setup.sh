#!/bin/bash

# Install the fish shell and make it the default shell.
paru -Syy fish
chsh -s `which fish`

# Install awesome window manager and create a local copy of the config file
paru -Syy awesome-git

# Install the compositor for the window manager
paru -Syy picom-jonaburg-git

# Telegram - messages app
paru -Syy telegram-desktop-bin

# Devilspie 2 - a program which can detect windows as they are created, and perform actions on them if they match as set of criteria
paru -Syy devilspie2

# Rofi - launcher
paru -Syy rofi

# Alacritty - terminal emulator
paru -Syy alacritty

# Kitty - terminal emulator
paru -Syy kitty

# Ranger - terminal file manager
paru -Syy ranger dragon-drag-and-drop
git clone https://github.com/alexanderjeurissen/ranger_devicons ~/.config/ranger/plugins/ranger_devicons
pip3 install ueberzug

# Nemo - file manager
paru -Syy nemo
paru -Syy nemo-fileroller
paru -Syy nemo-image-converter
gsettings set org.cinnamon.desktop.default-applications.terminal exec kitty

# VSCodium - code editor
paru -Syy vscodium-bin

# Discord - messages and calls app
paru -Syy discord

# Xournal++ - note taking app for graphics tablet
paru -Syy xournalpp

# Neofetch - system viewer tool
paru -Syy neofetch

# Photoqt - simple image viewer
paru -Syy photoqt

# Xclip - command line clipboard tool
paru -Syy xclip

# Krita - digital art application
paru -Syy krita

# Blender - 3D modelling application
paru -Syy blender

# Gimp - image processing application
paru -Syy gimp

# Kdenlive - video editing application
paru -Syy kdenlive

# OBS - screen recording / broadcasting application
paru -Syy cef-minimal-obs
paru -Syy obs-studio-tytan652

# MPV - video player application
paru -Syy mpv

# htop and btop - command line system monitors
paru -Syy htop btop

# Brave - web browser
paru -Syy brave-bin

# MOC - command line music player
paru -Syy moc

# CAVA - command line music visualizer
paru -Syy cava

# rsync - cp replacement
paru -Syy rsync

# exa - ls replacement
paru -Syy exa

# jump - command line tool to navigate the filesystem
paru -Syy jump-bin

# pip - python package manager
paru -Syy python-pip

# Drivers for graphics tablet
paru -Syy digimend-kernel-drivers-dkms

# Some cool fonts
paru -Syy noto-fonts-emoji ttf-fira-code nerd-fonts-source-code-pro

# Run vscodium 
codium

# Configuration software
paru -Syy lightdm-gtk-greeter-settings

# Screenshot utility
paru -Syy flameshot-git

# timage - terminal image viewer
paru -Syy timg

# Better lockscreen
paru -Syy arcolinux-betterlockscreen

# bat - cat replacement
paru -Syy bat
